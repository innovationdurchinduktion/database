-- MySQL dump 10.16  Distrib 10.1.26-MariaDB, for Win32 (AMD64)
--
-- Host: localhost    Database: rntv
-- ------------------------------------------------------
-- Server version	10.1.26-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `curator`
--

DROP TABLE IF EXISTS `curator`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `curator` (
  `CID` int(11) NOT NULL AUTO_INCREMENT,
  `C_RFID` char(10) NULL UNIQUE,
  `fname` varchar(50) COLLATE latin1_german1_ci NOT NULL,
  `lname` varchar(50) COLLATE latin1_german1_ci NOT NULL,
  `email_address` varchar(50) COLLATE latin1_german1_ci NOT NULL,
  `password` char(32) COLLATE latin1_german1_ci NOT NULL,
  PRIMARY KEY (`CID`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1 COLLATE=latin1_german1_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

DROP TABLE IF EXISTS `item`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `item` (
  `IID` int(11) NOT NULL AUTO_INCREMENT,
  `I_RFID` char(10) NULL UNIQUE,
  `Name` tinytext COLLATE latin1_german1_ci NOT NULL,
  `Description` text COLLATE latin1_german1_ci,
  `isActive` tinyint(1) NOT NULL,
  `addedBy_CID` int(11) NOT NULL,
  `add_date` datetime NOT NULL,
  PRIMARY KEY (`IID`),
  KEY `item_curator_CID_fk` (`addedBy_CID`)
) ENGINE=InnoDB AUTO_INCREMENT=101 DEFAULT CHARSET=latin1 COLLATE=latin1_german1_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `rent`
--

DROP TABLE IF EXISTS `rent`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rent` (
  `RID` int(11) NOT NULL AUTO_INCREMENT,
  `CID` int(11) NOT NULL,
  `fname` varchar(50) COLLATE latin1_german1_ci NOT NULL,
  `lname` varchar(50) COLLATE latin1_german1_ci NOT NULL,
  `email_address` varchar(100) COLLATE latin1_german1_ci NOT NULL,
  `from_date` datetime NOT NULL,
  `due_date` datetime NOT NULL,
  `IID` int(11) NOT NULL,
  `returned_date` datetime,
  PRIMARY KEY (`RID`),
  KEY `Rent_curator_CID_fk` (`CID`),
  KEY `Rent_item_IID_fk` (`IID`)
) ENGINE=InnoDB AUTO_INCREMENT=51 DEFAULT CHARSET=latin1 COLLATE=latin1_german1_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-10-12 13:32:09
